# Dotfiles

I backup my dotfiles and other configs to this repository.

## Most Important Configs

- [Awesome](https://gitlab.com/orasponka/dotfiles/-/tree/main/.config/awesome)
- [Alacritty](https://gitlab.com/orasponka/dotfiles/-/tree/main/.config/alacritty)
- [Bash](https://gitlab.com/orasponka/dotfiles/-/blob/main/.bashrc)
- [Conky](https://gitlab.com/orasponka/dotfiles/-/tree/main/.conky)
- [Doom Emacs](https://gitlab.com/orasponka/dotfiles/-/blob/main/.doom.d/)
- [Fish](https://gitlab.com/orasponka/dotfiles/-/tree/main/.config/fish)
- [Termite](https://gitlab.com/orasponka/dotfiles/-/tree/main/.config/termite)
- [Qtile](https://gitlab.com/orasponka/dotfiles/-/tree/main/.config/qtile)

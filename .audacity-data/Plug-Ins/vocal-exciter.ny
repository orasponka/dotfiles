;nyquist plug-in
;version 1
;type process
;name "Vocal Exciter"
;action "Getting Excited !"
;info "Vocal Exciter v.1.0 - by Steve Daulton\nhttp://audacity.easyspacepro.com\nInspired by the Aural Exciter by Aphex Systems.\n\nRequires tracks to be 32 bit / 44.1kHz or above.\nAdds harmonically related distortion to high frequencies\nto give more pressence.\n\nAlthough originally designed for vocal tracks,\nit may be used on other types of audio."

;control mix "(amount of effect)   Dry" real "Wet" 0.2 0.0 1.0

(defun excite (s-in)
;; Create harmonics
(setq harmonics (scale 10.0 (hp (highpass8 (s-abs (scale 4.0 (highpass4 s-in 5000))) 2000) 20000)))
;; Shape harmonics
(hp (eq-band (eq-band harmonics 4000 -12 0.5) 2500 -12 1.0) 5000))

;; Mix it
(if (arrayp s)(vector
(sim (aref s 0) (mult mix (excite (aref s 0)) -1))
(sim (aref s 1) (mult mix (excite (aref s 1)) -1)))
(sim s (mult mix (excite s) -1)))



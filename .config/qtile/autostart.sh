#!/usr/bin/env bash

picom &
nitrogen --restore
nm-applet &
volumeicon &
/usr/bin/emacs --daemon &
udiskie -t &
unclutter &
clipit &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
blueberry-tray &
dunst &
cbatticon -n -i notification &
mailspring --background &

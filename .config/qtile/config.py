# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import psutil
import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
#terminal = guess_terminal()

mod = "mod4" # Win/Super Key
terminal = "alacritty"
browser = "firefox"
browser2 = "brave"
editor = "emacsclient -c -a emacs"
terminaleditor = "vim"
filemanager = "pcmanfm"
wallpapermanager = "nitrogen"
screenshotmanager = "flameshot gui"
gaps = 5
border_width = 2
border_normal = "#000000"
border_focus = "#808080"
fontsize = 11
font = "Ubuntu"
padding = 3
location = "65.1763737,25.3532265"
wttrin_location = "Haukipudas"
lang = "fi"
bar_bg = "#3a3a3a"
bar_opacity = 0.90


keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    ### Custom Keybinds
    Key([mod], "y", lazy.spawn(editor), desc="text editor"),
    Key([mod], "v", lazy.spawn(filemanager), desc="filemanager"),
    Key([mod], "b", lazy.spawn(browser), desc="browser"),
    Key([mod, "control"], "b", lazy.spawn(browser2), desc="browser"),
    Key([mod], "w", lazy.spawn(wallpapermanager), desc="wallpapermanager"),
    Key([mod], "p", lazy.spawn("filelight"), desc="filelight"),
    Key([mod], "q", lazy.spawn(terminal + " -e" + terminaleditor), desc="terminal texteditor"),
    Key([mod], "g", lazy.spawn("pavucontrol"), desc="vim"),

    Key([mod], "f", lazy.window.toggle_fullscreen(), desc="toggle fullscreen"),
    Key([mod, "control"], "f", lazy.window.toggle_floating(), desc="toggle fullscreen"),

    Key([], "Print", lazy.spawn(screenshotmanager), desc="screenshot tool"),

    ### Brightness controls
    Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl s +5%")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl s 5%- ")),

    ### Audio controls
    Key(
        [], "XF86AudioRaiseVolume",
        lazy.spawn("amixer -q set Master 5%+")
    ),
    Key(
        [], "XF86AudioLowerVolume",
        lazy.spawn("amixer -q set Master 5%-")
    ),
    Key(
        [], "XF86AudioMute",
        lazy.spawn("amixer -q set Master toggle")
    ),


    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "c", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawn("dmenu_run"), desc="Run dmenu_run"),
    Key([mod], "d", lazy.spawn("dm-hub"), desc="Run dm-hub"),
]

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

layouts = [
    layout.Columns(border_normal=border_normal,border_focus=border_focus, border_width=border_width, margin = gaps),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    #layout.Bsp(),
    #layout.Matrix(margin = gaps),
    #layout.MonadTall(border_focus="#215578", border_width=2, margin = gaps),
    #layout.MonadWide(border_normal="#000000",border_focus="#215578", border_width=border_width, margin = gaps),
    layout.RatioTile(border_normal=border_normal, border_focus=border_focus, border_width=border_width, margin = gaps),
    #layout.Tile(border_focus="215578", border_width=2, margin = gaps),
    #layout.TreeTab(),
    #layout.VerticalTile(margin = gaps),
    #layout.Zoomy(),
    layout.Max(),
]

widget_defaults = dict(
    font=font,
    fontsize=fontsize,
    padding=padding,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Spacer(length=-3),
                widget.CurrentLayoutIcon(scale=0.65),
                widget.Spacer(length=-2),
                widget.GroupBox(
                    inactive = "#d9dedb",
                    fontsize = 10,
                    borderwidth = border_width,
                    padding_x = 5
                ),
                ##widget.Prompt(),
                widget.Spacer(),
                #widget.WindowName(format = ""),
                #widget.Chord(
                #    chords_colors={
                #        "launch": ("#ff0000", "#ffffff"),
                #    },
                #    name_transform=lambda name: name.upper(),
                #),
                #widget.TextBox("default config", name="default"),
                #widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                widget.Systray(icon_size = 15, padding = 6),
                widget.Spacer(length = 5),
                widget.CPU(format = "{load_percent}%",  mouse_callbacks = {"Button1": lambda: qtile.cmd_spawn(terminal + " -e htop")}),
                widget.ThermalZone(update_interval = 1, high=65, crit = 85,  mouse_callbacks = {"Button1": lambda: qtile.cmd_spawn(terminal + " -e htop")}),
                widget.Spacer(length = -3),
                widget.Memory(
                     mouse_callbacks = {"Button1": lambda: qtile.cmd_spawn(terminal + " -e htop")}
                ),
                widget.Spacer(length = -4),
                widget.Clock(format=" %d.%m.%Y %a %H:%M"),
                widget.Wttr(
                    location={location : ""},
                    format = "%c%C %t  %f",
                    lang = lang,
                    update_interval = 300,
                    mouse_callbacks = {"Button1": lambda: qtile.cmd_spawn(terminal + " --hold -e curl -q wttr.in/" + wttrin_location)}
                ),
                widget.Spacer(length = 2),
               # widget.QuickExit(
               #     default_text = "[ exit qtile ]",
               #     countdown_format = "[    {} sec    ]",
               # ),
            ],
            24,
            background=bar_bg,
            opacity = bar_opacity,
            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ],
    border_focus = border_focus,
    border_normal = border_normal,
    border_width = border_width
)

auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.run([home])

if status is-interactive
    # Commands to run in interactive sessions can go here
end

function clean
    sudo journalctl --vacuum-size=200M
    rm -rf ~/.cache/*
    sudo pacman -Sc
    sudo pacman -Scc
    yay -Scc
    yay -Sc
end

# Functions needed for !! and !$
function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end
# The bindings for !! and !$
if [ $fish_key_bindings = "fish_vi_key_bindings" ];
  bind -Minsert ! __history_previous_command
  bind -Minsert '$' __history_previous_command_arguments
else
  bind ! __history_previous_command
  bind '$' __history_previous_command_arguments
end


## Aliases ##

### Package Manager and System ###
alias pacmansyu="sudo pacman -Syyu"
alias yaysua="yay -Sua --noconfirm"
alias unlock="sudo rm /var/lib/pacman/db.lck"

alias vacuum="sudo journalctl --vacuum-size=200M"

### cd aliases  ###
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias home="cd ~"

### ls/exa Aliases ###
alias ls="exa -lah --color=always --group-directories-first"
alias lsless="exa -lah --color=always --group-directories-first | less -R" 
alias l.='exa -a | egrep "^\."'
alias lsd='/usr/bin/ls'

### Brightnessctl  ####

alias b1="brightnessctl set 100%"
alias b8="brightnessctl set 80%"
alias b6="brightnessctl set 60%"
alias b5="brightnessctl set 50%"
alias b3="brightnessctl set 30%"

### Other Aliases  ###
alias c="clear"
alias cb="clear && fish && exit"
alias cbh="cd && clear && fish && exit"

alias quit="exit"
alias q="exit"

alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"

alias doom="~/.emacs.d/bin/doom"

alias translate="trans --verbose -b"

alias ok="cd /run/media/orri/ok/"

alias mv="mv -i"
alias rm="rm -i"
alias cp="cp -i"

alias play-pause="playerctl play-pause"

alias grep="grep --color=auto"

# Libreoffice
set -x theme "Arc"
alias libreoffice="GTK_THEME=$theme command libreoffice"
alias loimpress="GTK_THEME=$theme command loimpress"
alias lowriter="GTK_THEME=$theme command lowriter"
alias lodraw="GTK_THEME=$theme command lodraw"
alias lobase="GTK_THEME=$theme command lobase"
alias localc="GTK_THEME=$theme command localc"
alias lomath="GTK_THEME=$theme command lomath"

# Fetch Master 6000
#fm6000 -m 5 --shell fish -color green -r -g 5

#Starship
starship init fish | source
